#!/usr/bin/env python3

import argparse
import requests

def check_domain_availability(api_key, domain_name):
	# TransIP NL API endpoint URL
	API_URL = f"https://api.transip.nl/v6/domain-availability/{domain_name}"

	# Headers with API key
	headers = {
		"Authorization": f"Bearer {api_key}",
	}

	# Make the API request
	response = requests.get(API_URL, headers=headers)

	# Check if the response is successful
	if response.status_code == 200:
		result = response.json()
		domain_status = result['availability']['status']

		# Map status to more concise output
		if domain_status == 'inyouraccount':
			print("notfree")
		else:
			print(domain_status)
	else:
		print("unavailable")

if __name__ == "__main__":
	# Create an argument parser
	parser = argparse.ArgumentParser(description="Check domain availability using the TransIP API")

	# Define command line arguments
	parser.add_argument("--api", dest="api_key", metavar="api_key", type=str, required=True, help="TransIP API key")
	parser.add_argument("--domain", dest="domain_name", metavar="domain_name", type=str, required=True, help="Domain name to check")

	# Parse the command line arguments
	args = parser.parse_args()

	api_key = args.api_key
	domain_name = args.domain_name

	# Check the availability of the domain
	check_domain_availability(api_key, domain_name)
