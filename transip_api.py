#!/usr/bin/env python3

import time
import argparse
import os
import requests
import json
import base64
from datetime import datetime

transip_api_url = "https://api.transip.nl/v6"

# Check if the 'pycryptodome' package is installed
try:
	from Crypto.Signature import pkcs1_15
	from Crypto.Hash import SHA512
	from Crypto.PublicKey import RSA
except ImportError:
	print("The 'pycryptodome' package is not installed. Install it using 'pip3 install pycryptodome'")
	exit(1)

def test_transip_api(api_key):
	url = f"{transip_api_url}/api-test"
	headers = {
		"Authorization": f"Bearer {api_key}"
	}
	
	try:
		response = requests.get(url, headers=headers)
		if response.status_code == 200:
			return True, "*** TransIP API key _valid_"
		elif response.status_code == 401:
			return False, "*** TransIP API key is _invalid_"
		else:
			return False, f"*** Received unexpected status code from TransIP: {response.status_code}"
	except Exception as e:
		return False, f"An error occurred: {e}"

# Function to convert expiry time to seconds
def convert_expiry_to_seconds(expiry):
	if expiry[-1] == 's':
		return int(expiry[:-1])
	elif expiry[-1] == 'm':
		return int(expiry[:-1]) * 60
	elif expiry[-1] == 'h':
		return int(expiry[:-1]) * 3600
	elif expiry[-1] == 'd':
		return int(expiry[:-1]) * 86400
	elif expiry[-1] == 'w':
		return int(expiry[:-1]) * 604800
	else:
		return int(expiry)

# Argument parser
parser = argparse.ArgumentParser(description='Generate TransIP API token.')
parser.add_argument('--username', required=True, help='TransIP username')
parser.add_argument('--expiry', default='30m', help='Token expiry time (default is 30 minutes)')
parser.add_argument('--readonly', action='store_true', help='Set the token to read-only')
parser.add_argument('--label', default=f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", help='Custom label for the token')
parser.add_argument('--test', help='API token we want to test')
parser.add_argument('--debug', action='store_true', help='Enable debug output')

args = parser.parse_args()

# Debug: Print parsed arguments
if args.debug:
	print(f"Debug: Parsed arguments - Username: {args.username}, Expiry: {args.expiry}, ReadOnly: {args.readonly}, Label: {args.label}")

# Convert expiry time to seconds
expiry_seconds = convert_expiry_to_seconds(args.expiry)

# Debug: Print converted expiry time
if args.debug:
	print(f"Debug: Converted expiry time to seconds: {expiry_seconds}")

# Check if expiry time is more than 1 month
if expiry_seconds > 2592000:
	print("Error: The maximum expiry time is 1 month (2592000 seconds).")
	exit(1)

if args.test:
	api_ok, api_status = test_transip_api(args.test)
	print(f"\n{api_status}")
	exit(1)

# Read the private key
with open("priv.key", "r") as f:
	private_key = f.read()

# Debug: Print first 50 characters of private key to verify it's being read correctly
if args.debug:
	print(f"Debug: First 50 characters of private key: {private_key[:50]}")

# Create the request body
body = {
	"login": args.username,
	"nonce": str(time.time()),
	"read_only": args.readonly,
	"expiration_time": f"{expiry_seconds} seconds",
	"label": args.label,
	"global_key": True
}

# JSON encode the request body
body_json = json.dumps(body)

# Debug: Print JSON body
if args.debug:
	print(f"Debug: JSON body: {body_json}")

# Create a SHA-512 hash of the request body
hash_object = SHA512.new(body_json.encode('utf-8'))

# Sign the hash with the private key
private_key_obj = RSA.import_key(private_key)
signature = pkcs1_15.new(private_key_obj).sign(hash_object)

# Base64 encode the signature
signature_base64 = base64.b64encode(signature).decode('utf-8')

# Debug: Print Base64 signature
if args.debug:
	print(f"Debug: Base64 signature: {signature_base64}")

# Make the POST request to TransIP
response = requests.post(f"{transip_api_url}/auth",
	json=body,
	headers={"Signature": signature_base64}
)

# Debug: Print the response
if args.debug:
	print(f"Debug: TransIP API response: {response.json()}")

# Check if 'token' exists in the response
if 'token' in response.json():
	print(response.json()['token'])
elif 'error' in response.json():
	print(f"Error: {response.json()['error']}")
else:
	print("Error: Token not found in response.")
	if args.debug:
		print(f"Debug: Full response: {response.json()}")
