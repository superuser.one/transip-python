# TransIP Python scripts

## transip_api.py

### What

Generates a Bearer Token using the Key Pair/Private Key from TransIP. 

### Installation

- `pip3 install pycryptodome requests` or `apt install -y python3-pycryptodome python3-requests`

### Get a Key Pair (Private Key)

- https://www.transip.be/cp/account/api/ (or transip.eu or transip.nl -- the API tokens work on any domain, but you can only login via the CP on the domain you signed up on)
- Put the private key in `priv.key` in this repo

### Usage

```
$ ./transip_api.py -h
usage: transip_api.py [-h] --username USERNAME [--expiry EXPIRY] [--readonly] [--label LABEL] [--test TEST] [--debug]

Generate TransIP API token.

optional arguments:
  -h, --help           show this help message and exit
  --username USERNAME  TransIP username
  --expiry EXPIRY      Token expiry time (default is 30 minutes)
  --readonly           Set the token to read-only
  --label LABEL        Custom label for the token
  --test TEST          API token we want to test
  --debug              Enable debug output
```

- Username is your TransIP username
- Expiry is expiry (in seconds, or complete with s, m, d or w for seconds, minutes, days and weeks respectively). Maximum is 4 weeks.
- Test <existing API key> will test whether the key is (still) valid.

## whois.py

### What

Provides simple output to check whether a domain is available or not using the TransIP API. Only TLDs supported by TransIP can be checked. 

Possible outputs are:
- `free`
- `notfree` (includes `inyouraccount`)
- `unavailable` (not supported by TransIP or something else went wrong)

### Generate Token

- Get a Bearer Token via `transip_api.py`

### Usage

```
$ ./whois.py
usage: whois.py [-h] --api api_key --domain domain_name
whois.py: error: the following arguments are required: --api, --domain
```

- api_key is the API token generated with `transip_api.py` or manually via https://www.transip.be/cp/account/api/
- domain is a domain to check (duh)